FROM node:18

WORKDIR /app
ADD . /app
RUN npm ci

EXPOSE 3000

CMD ["npm", "run", "start"]

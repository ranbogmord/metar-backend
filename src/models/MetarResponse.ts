export interface WeatherConditions {
    code: string;
}

export interface CloudData {
    code: string;
    base_feet_agl: number;
    base_meters_agl: number;
}

export interface WindData {
    degrees: number;
    speed_kts: number;
    speed_mps: number;
    gust_kts: number;
    gust_mps: number;
}

export interface VisibilityData {
    miles: string;
    miles_float: number;
    meters: string;
    meters_float: number;
}

export interface TemperatureData {
    celsius: number;
    fahrenheit: number;
}

export interface DewpointData {
    celsius: number;
    fahrenheit: number;
}

export interface BarometerData {
    hg: number;
    kpa: number;
    mb: number;
}

export interface WeatherReport {
    raw_text: string;
    raw_parts: string[];
    icao: string;
    observed: string;
    wind: WindData;
    visibility: VisibilityData;
    conditions: WeatherConditions[];
    clouds: CloudData[];
    temperature: TemperatureData;
    dewpoint: DewpointData;
    humidity_percent: number;
    barometer: BarometerData;
    flight_category: string;
}
import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { WeatherReport } from './models/MetarResponse';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get("/metar/:icaos")
  async getMetar(@Param('icaos') icaos: string): Promise<WeatherReport[]> {
    const metars = await this.appService.getMetar(icaos.split(','));
    return metars;
  }
}

import { Injectable } from '@nestjs/common';
import { WeatherReport } from './models/MetarResponse';
import * as metarParser from 'adh-metar-parser';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getMetar(icaos: string[]): Promise<WeatherReport[]> {
    const res = await fetch(`https://metar.vatsim.net/${icaos.join(',')}`);
    const rawMetars = await res.text();
    const metars = rawMetars.split("\n").map(metar => {
      return metarParser(metar) as WeatherReport;
    });

    return metars;
  }
}
